from distutils.util import convert_path
from os import path

from setuptools import setup, find_packages

here = path.abspath(path.dirname(__file__))
metadata = dict()
with open(convert_path('systems_netsuite/version.py')) as metadata_file:
    exec(metadata_file.read(), metadata)

setup(
    name='systems-netsuite',

    version=metadata['__version__'],
    zip_safe=False,

    description='Library for netsuite communication',

    author='Alexey Hurko',
    author_email='alexey.hurko@formlabs.com',

    license='EULA',

    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'scripts']),

    install_requires=[
        'netsuite',
        'requests',
        'xmltodict',
        'zeep'
    ],
    setup_requires=[
        'pytest-runner'
    ],
    tests_require=[
        'pytest'
    ],

    include_package_data=True,

    extras_require={
        'test': ['coverage', 'pytest'],
    },
)
