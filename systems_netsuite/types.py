# TODO: add all objects
TRANSACTIONS = [
    'sales',
    'purchases'
]

LISTS = [
    'relationships',
    'marketing',
    'employees',
    'accounting'
]

PLATFORM = [
    'common',
    'core',
    'faults',
    'messages'
]
