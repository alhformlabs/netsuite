COMMON = [
    'Address',
    'InventoryDetail',
    'LandedCost'
]

CORE = [
    'Passport',
    'SsoPassport',
    'TokenPassport'
]

SALES = [
    'AccountingTransactionSearch',
    'AccountingTransactionSearchAdvanced',
    'Opportunity',
    'OpportunitySearch',
    'OpportunitySearchAdvanced',
    'SalesOrder',
    'SalesOrderItem',
    'TransactionSearch',
    'TransactionSearchAdvanced',
    'ItemFulfillment',
    'Invoice',
    'CashSale',
    'Estimate'
]

PURCHASES = [
    'VendorBill',
    'PurchaseOrder',
    'PurchaseOrderItem'
    'ItemReceipt',
    'VendorPayment',
    'VendorCredit',
    'Purchase/Requisition',
    'InboundShipment'
]

RELATIONSHIPS = [
    'Billing Account',
    'Contact',
    'ContactSearch',
    'ContactSearchAdvanced',
    'Customer',
    'CustomerSearch',
    'CustomerSearchAdvanced',
    'CustomerStatus',
    'CustomerSubsidiaryRelationship',
    'Partner',
    'PartnerSearch',
    'PartnerSearchAdvanced',
    'Vendor',
    'VendorSearch',
    'VendorSearchAdvanced',
    'VendorSubsidiaryRelationship',
    'EntityGroup',
    'EntityGroupSearch',
    'EntityGroupSearchAdvanced',
    'Job',
    'JobSearch',
    'JobSearchAdvanced',
    'JobType',
    'JobStatus'
]

ACCOUNTING = [
    'GiftCertificateSearch',
    'GiftCertificateSearchAdvanced',
    'GlobalAccountMapping',
    'Inventory Number',
    'ItemAccountMapping',
    'Location',
    'LocationSearch',
    'LocationSearchAdvanced',
    'Nexus',
    'Revenue Recognition Schedule',
    'Revenue Recognition Template',
    'Sales Tax ItemItemSearchAdvanced',
    'ContactRole',
    'Bin',
    'SalesTaxItem',
    'TaxAcct',
    'TaxGroup',
    'TaxType',
    'Subsidiary',
    'SubsidiarySearch',
    'SubsidiarySearchAdvanced',
    'UnitsType',
    'PartnerCategory',
    'VendorCategory'
]
